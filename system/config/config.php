<?php

define('DOMAIN', 'http://' . $_SERVER['SERVER_NAME']);
define('SITE_NAME', 'Phone Book');

define('CORE', HOME_DIR . '/system/core');
define('LIB', HOME_DIR . '/system/lib');
define('CONFIG', HOME_DIR . '/system/config');

define('APP', HOME_DIR . '/app');
define('DIR_ROUTES', HOME_DIR . '/routes');


define('SKIN', 'general');
define('URL_VIEW', '/view/'.SKIN);
define('DIR_VIEW', APP.URL_VIEW);
define('LAYOUT', 'main');

define('D_S', DIRECTORY_SEPARATOR);

define('LOGIN_URL', 'login');
define('REG_URL', 'reg');
define('AUTH_USER', 'userAuth');

define('COLOR_REPORT', 'text-muted');
define('COLOR_ERROR', 'text-danger');

define('LENG_DIR', APP.'/lengs/ru');


