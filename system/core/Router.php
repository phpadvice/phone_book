<?php
namespace system\core;

/**
 *
 */
class Router
{
    protected static $flagRoute = false;

    protected static $route;

    protected static $url;

    public static function getRoute()
    {
        return self::$route;
    }

    public static function removeQueryString($url)
    {

        if ($url) {

            $params = explode('&', $url, 2);

            if (false === strpos($params[0], '=')) {

                self::$url = rtrim($params[0], '/');

            }

        }

    }

    public static function run($url)
    {
// debug(self::$route);
        self::removeQueryString($url);

// debug(self::$route);

        self::loadRoutRule('main');
// debug(self::$route);
        self::launcherController();

        // self::loginRedirect();
    }

    protected static function getArrRule($patternIn)
    {

//сделать проверку изменился или нет filemtime
        // если отличаются getфайл и файл массива больше чем на N минут то обновить
        $pattern = preg_replace('#\{([^\d][^\}]*)\}#i', '(?P<\1>[^/]*)', $patternIn);

        $pattern = str_replace('/', '\/', $pattern);

        return $pattern;

    }

    protected static function loverCamelCase($name)
    {

        $name = lcfirst(self::upperCamelCase($name));

        return $name;
    }

    protected static function upperCamelCase($name)
    {
        $name = str_replace('-', ' ', $name);

        $name = ucwords($name);

        $name = str_replace(' ', '', $name);

        return $name;
    }

    private static function get($pattern, $route = [])
    {

        if (false === self::$flagRoute) {

            self::matchRoute($pattern, $route);
        }

    }

    private static function launcherAction($cObj, $action)
    {

        if (method_exists($cObj, $action)) {

            $cObj->$action();

            $cObj->runView();
        } else {

            echo '<br>Метод - ', self::$route['controller'], '::', $action, ' не найден';
        }

    }

    private static function launcherController()
    {

        if (true === self::$flagRoute) {

            $controller = 'app\controllers\\' . self::$route['controller'] . 'Control';

            if (class_exists($controller)) {

                $cObj = new $controller(self::$route);

                $action = self::loverCamelCase(self::$route['action']) . 'Action';

                self::launcherAction($cObj, $action);
            } else {

                echo 'Нет контроллера - ', $controller;
            }

        } else {

            err404();
        }

    }

    private static function loadRoutRule($fileName)
    {

        if ( ! empty($_POST)) {

            $mothod = 'Post';

        } else {

            $mothod = 'Get';

        }

        require_once DIR_ROUTES . D_S . $fileName . $mothod . '.php';
    }

    private static function loginRedirect($ctrl = false)
    {
        $ctrl = $ctrl ?: self::$url;
// debug(self::$route);die;

        if ( ! empty($_SESSION[AUTH_USER])) {

            if (LOGIN_URL == $ctrl) {

                redirect('/');
            }

            return true;
        } else {

            if ( ! $ctrl

                or LOGIN_URL != $ctrl

                and REG_URL != $ctrl

                //AND 'ajax'!=$ctrl
            ) {

                redirect('/' . LOGIN_URL . '/');
            }

        }

    }

    private static function matchRoute($pattern, $route = [])
    {
        $pattern = self::getArrRule($pattern);

        if (preg_match("#^$pattern$#i", self::$url, $matches)) {

            foreach ($matches as $key => $val) {

                if (is_string($key)) {

                    $route[$key] = $val;
                }

            }

            self::$route = $route;

            self::$flagRoute = true;
        }

    }

    private static function post($pattern, $route = [])
    {

        if (false === self::$flagRoute) {

            self::matchRoute($pattern, $route);
        }

    }

}
