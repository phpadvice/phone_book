<?php

namespace system\core;

/**
 *
 */
abstract class Controller
{
    // 1-шаблон и контент
    // 2-только контент

    const TYPE_CONT = 2;

    /**
     * режимы вывода
     */
    const TYPE_FULL = 1;

    const TYPE_JSON = 3;

    protected $layout;

    // 3-только json

    protected $route;

    protected $typeRender = self::TYPE_FULL;

    protected $vars = [];

    protected $view;

    public function __construct($route)
    {

        $this->route = $route;

        $this->view = $route['action'];

        // $this->layout ='test';
    }

    public function add($vars)
    {

        foreach ($vars as $key => $value) {

            $this->vars[$key] = $value;
        }

    }

    public function runView()
    {

        $objectView = new View($this->route, $this->layout, $this->view);

        $objectView->runView($this->vars, $this->typeRender);
    }

    public function set($vars)
    {
        $this->vars = $vars;
    }

    public function setErr(string $strErr)
    {
        $this->vars['error'] = $strErr;
    }

    public function setTypeRender($type)
    {
        $this->typeRender = $type;
    }

}
