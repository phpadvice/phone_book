<?php
namespace system\core;

use system\core\DataBase;
use system\lib\Validation;

class Model
{
    public $field = 'id';

    public $formData = [];

    public $table;

    protected $error = [];

    protected $params = [];

    protected $rules = [];

    public function __construct()
    {
        $this->db = DataBase::singlDB();
    }

    /**
     * [errDbError description]
     *
     * @return [type] [description]
     */
    public function errDbError()
    {
        $error = require_once LENG_DIR . '/error_db.php';

        $key = $this->db->error[1];

        $mesage = $this->db->error[2];

        return isset($error[$key]) ? $error[$key] : 'Ошибка: ' . $key . ' - ' . $mesage;

        // return $this->db->error[2];
    }

    /**
     * Возвращает сообщение об ошивке из массива ошибок
     * @return string
     */
    public function errDefMessage()
    {
        return ! empty($this->error[2]) ? $this->error[2] : 'действие вернуло ошибку';
    }

    /**
     * [errLengMessage description]
     *
     * @param  integer $flag [description]
     *
     * @return [type]        [description]
     */
    public function errLengMessage($flag = 0)
    {
        $error = require_once LENG_DIR . '/error.php';

        $key = $this->error[0] . '_' . $this->error[1];

        return isset($error[$key]) ? $error[$key] : 'Ошибка: ' . $key;
        // if($flag) {return $key;
        // return isset($error[$key]) ? $error[$key] : $this->errDefMessage();
    }

    public function findOne($id, $field = '')
    {
        $field = $field ?: $this->pk;
        $sql   = "SELECT * FROM {$this->table} WHERE $field = ? LIMIT 1";
        // die($sql);
        return $this->db->sql($sql, [$id]);
    }

    /**
     * [getError description]
     * @return [type] [description]
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * [getFormData description]
     * @return [type] [description]
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * [upload description]
     *
     * @param  array  $data [description]
     *
     * @return [type]       [description]
     */
    public function upload($data = [])
    {
        $data = $data ?: $_POST;

        foreach ($this->formData as $name => $value) {

            if (isset($data[$name])) {
                $this->formData[$name] = $data[$name];
            }

        }

    }

    /**
     * [validateForm description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function validateForm($data = [])
    {
        $data = $data ?: $this->formData;

        $valid = new Validation($data);

        if (false === $valid->rules($this->rules)) {
// debug();
            $this->error = $valid->getError();

            return false;
        }

        return true;

    }

    protected function insert($data)
    {
        $arr = $this->dataToInsert($data);

        $sql = "INSERT INTO `" . $this->table .
            "`(" . $arr['fields'] . ") VALUES (" . $arr['values'] . ");";

// debug('params[]',$this->params);die($sql);
        return $this->db->insert($sql, $this->params);
    }

    protected function select($data)
    {
        $strWhere=$this->dataToUSelect($data);
        $sql   = "SELECT * FROM {$this->table} WHERE $strWhere LIMIT 1";
    // debug($this->params);die($sql);
        return $this->db->sql($sql, $this->params);
    }

    protected function update($data)
    {
        $arr = $this->dataToUpdate($data);

        $sql = "UPDATE `" . $this->table .
            "` SET " . $arr['fields'] . " WHERE " . $arr['where'] . ";";
// debug('params[]', $this->params);die($sql);
        $this->db->sql($sql, $this->params);
    }

    private function dataToInsert($data)
    {

        foreach ($data as $field => $value) {

            $arrFields[] = "`$field`";

            if (empty($value[1])) {

                $arrValues[] = '?';

                $this->params[] = $value[0];
            } else {

                $arrValues[] = $value[0];
            }

        }

        $arr['fields'] = implode(', ', $arrFields);
        $arr['values'] = implode(', ', $arrValues);

        return $arr;
    }

    private function dataToUpdate($arr)
    {
        $arrParams['fields'] = [];
        $arrParams['where']  = [];

        foreach ($arr as $arrName => $arrData) {

            foreach ($arrData as $field => $value) {

                if (empty($value[1])) {
                    $arrField[]            = "`$field`=?";
                    $arrParams[$arrName][] = $value[0];
                } else {
                    $arrField[] = "`$field`='{$value[0]}'";
                }

            }

            $arr[$arrName] = implode(',', $arrField);
            $arrField      = [];

        }

        $this->params = array_merge($arrParams['fields'], $arrParams['where']);

        return $arr;
    }

    private function dataToUSelect($arrWhere)
    {

// debug($arrWhere);
            foreach ($arrWhere as $field => $value) {

                if (empty($value[1])) {
                    $arrField[]            = "`$field`=?";
                    $this->params[] = $value[0];
                } else {
                    $arrField[] = "`$field`='{$value[0]}'";
                }

            }

            $strWhere = implode(' AND ', $arrField);

        return $strWhere;
    }

}
