<?php

/**
 * Автозагрузка классов
 */
spl_autoload_register(function ($class) {

    $file = HOME_DIR . D_S . str_replace("\\", D_S, $class) . '.php';

    if (is_file($file)) {
        require_once $file;
    }

});
