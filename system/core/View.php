<?php

namespace system\core;

/**
 *
 */
class View
{
    public $layout;

    public $route = [];

    public $view;

    private static $meta = [];

    public function __construct($route, $layout = '', $view = '')
    {
        $this->route = $route;

        $this->layout = $layout ?: LAYOUT;

        $this->view = $view;
    }

    public static function getMeta()
    {
        echo '<title>' . self::$meta['title'] . '</title>
      <meta name="description" content="' . self::$meta['desc'] . '">
      <meta name="keywords" content="' . self::$meta['keyword'] . '">';
    }

    /**
     * [runView description]
     * @param  [type] $vars       [description]
     * @param  [type] $typeRender - режимы вывода
     * 1-шаблон и контент
     * 2-контент
     * 3-json
     * @return [type]             [description]
     */
    public function runView($vars, $typeRender)
    {
        $typeRender = $typeRender ?: 1;

        $fileContent = DIR_VIEW . "/{$this->route['controller']}/{$this->view}.php";

        if (1 == $typeRender) {

            $fileLayout = DIR_VIEW . "/layout/{$this->layout}.php";

            self::render($fileLayout, $vars, $fileContent);
        } elseif (2 == $typeRender) {

            self::render($fileContent, $vars);
        } elseif (3 == $typeRender) {

            $vars['err'] = null;

            echo json_encode($vars);
        }

    }

    public static function setMeta($title, $desc = '', $keyword = '')
    {
        self::$meta['title']   = $title;
        self::$meta['desc']    = $desc;
        self::$meta['keyword'] = $keyword;
    }

    private function render($fileRender, $vars, $fileContent = null)
    {

        $error = '';

        if ( ! is_array($vars)) {

            die('данные для страницы отсутствуют');
        }
// debug($vars);
        extract($vars, EXTR_OVERWRITE, 'tmp_');

        if ('' != $error) {

            $color_report = COLOR_ERROR;

            $report = '';
        }

        require_once $fileRender;

    }

}
