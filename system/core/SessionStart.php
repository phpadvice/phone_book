<?php

/**
 * если пришел Бот то сессию не включаем
 */
if (strpos($_SERVER['HTTP_USER_AGENT'], 'bot')) {

    if (!empty($_GET['PHPSESSID'])) {

        header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found");

        exit();

    }
}

/**
 * запуск сессии
 */
@session_start();
