<?php
namespace system\core;

use \PDO;

/**
 *
 */
class DataBase
{
    /**
     * @var mixed
     */
    public $error;

    /**
     * @var array
     */
    private $arrSql = [];

    /**
     * @var mixed
     */
    private static $db = null;

    /**
     * @var mixed
     */
    private $pdo;

    public function __construct()
    {
        require_once HOME_DIR . '/system/config/db.php';

        $this->pdo = new PDO(
            "mysql:host=" . $db_host . ";port=3306;dbname=" . $db_name,
            $db_l,
            $db_p
        );

        $this->pdo->query("SET lc_time_names = $time_names");

        $this->pdo->query("SET NAMES $charset");
    }

    public function __destruct()
    {
        $stmt = null;

        $this->pdo = null;

    }

    /**
     * получить id после вставки новых данных
     * @param  [type] $sql    [description]
     * @param  array  $params [description]
     * @return [type] $newId       [integer]
     */
    public function insert($sql, $params = [])
    {
        $this->sql($sql, $params);

        $newId = $this->pdo->lastInsertId();

        if (empty($newId)) {
            return false;
        }

        return $newId;
    }

    /**
     * без получения данных
     * @param  [type] $sql    [description]
     * @param  array  $params [description]
     * @return [type] errors  [string]
     */
    public function query($sql, $params = [])
    {
        $stmt = $this->pdo->prepare($sql);

        return $stmt->execute($params);
    }

    /**
     * одиночка
     * @return [type] [description]
     */
    public static function singlDB()
    {

        if (null == self::$db) {
            self::$db = new DataBase();
        }

        return self::$db;
    }

    /**
     * получение данных
     * @param  [type] $sql    [description]
     * @param  [type] $params [description]
     * @return [type]         [array]
     */
    public function sql($sql, $params = [])
    {
        $stmt = $this->pdo->prepare($sql);
        $res  = $stmt->execute($params);

        if (false !== $res) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $this->error = $stmt->errorInfo();
        }

    }

}
