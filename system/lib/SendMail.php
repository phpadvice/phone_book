<?php
namespace system\lib;


/**
 *
 */
class SendMail
{


/**
 * [sendMail description]
 * @param  [type] $email     [description]
 * @param  [type] $link_hesh [description]
 * @return [type]            [description]
 */
  public function sendMail($email, $link_hesh){

    extract($this->loadMsgFile($link_hesh), EXTR_OVERWRITE);

    $to  = $email;

    $headers  ="MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: <".$mail_from.">\r\n";
    $headers .= "Reply-To: <".$mail_from.">\r\n";

    file_put_contents(HOME_DIR.'/mail_tmp.html', $to.'<br>'.$subject.'<br>'.$msg.'<br>'.$headers);
    // mail($to, $subject, $msg, $headers);

  }

  private function loadMsgFile($link_hesh){

    return require_once CONFIG.'/msg_mail.php';
  }

}