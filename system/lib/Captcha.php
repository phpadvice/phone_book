<?php
namespace system\lib;
/**
 *
 */
class Captcha
{


  function run(){

    if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }

    $img = $this->createCap();

    $this->no_cache();

    imagepng($img);

    imagedestroy($img);

  }

  function checkCap($cap_in){

    if($_SESSION['cap']==md5( $cap_in )) {

      return true;
    }

  }



  private function randStr($count=4) {

   // $base_str = 'abcdefghijklmnopqrstuvwxyz0123456789';

    $base_str = '0123456789';

    $rand_str = '';
    $count_str = strlen($base_str) - 1;

    for ($i = 0; $i < $count; $i++) {
        $rand_str .= $base_str[rand(0, $count_str)];
    }
    return $rand_str;
  }


  private function createCap() {

    $font = 'fonts/captcha.ttf';

    $rand_str = $this->randStr();

    $_SESSION['cap'] = md5($rand_str);

    $img = imagecreatetruecolor(112, 45);

    $wh = imagecolorallocate($img, 255, 255, 255);

    $gr = imagecolorallocate($img, 150, 150, 150);

    $bl = imagecolorallocate($img, 0, 0, 0);


    imagefilledrectangle($img, 0, 0, 20, 35, $bl);

    imagettftext($img, 22, 1, 15, 37, $gr, $font, $rand_str);

    imagettftext($img, 25, 3, 12, 35, $wh, $font, $rand_str);

    return $img;

  }


  private function no_cache() {

    header("Expires: Wed, 1 Jan 1997 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header ("Content-type: image/png");

  }



}

 ?>