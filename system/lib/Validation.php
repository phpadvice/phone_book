<?php
namespace system\lib;

/**
 *
 */
class Validation
{
    // static function filter_txt($data){
    //   $data=trim($data);
    //   $data = preg_replace('|[\s]+|s', ' ', $data);
    //   $data = stripslashes($data);
    //   $data = htmlspecialchars($data);
    //   $data=filter_var($data, FILTER_SANITIZE_STRING);
    //   return $data;
    // }

    // public $rules = [];
    public $data = [];

    public $error = [];

    private $strLen = 8;

    //длинна пароля по умолчанию

    public function __construct($data)
    {
        $this->data = $data;
        // $this->data['email']='test';
        // $this->data['pass']    = '-4ee654r';
        // $this->data['pass2']   = '-4ee654r';
        // $this->data['confirm'] = '-4ee654r';
    }

    public function getError()
    {

        return $this->error;
    }

    public function rules($rules = [])
    {

        if ([] === $rules) {

            $this->error = ['rules', 'empty'];

            return false;
        }

        foreach ($rules as $name => $value) {

            if ( ! method_exists($this, $name)) {

                $this->error = [$name, 'no_rules'];

                return false;
            }

            if (false === $this->$name($value)) {

                return false;
            }

        }

    }

    private function captha($list)
    {
        $cap = new Captcha;

        foreach ($list as $value) {

            $keyData = $value[0];

            if ( ! $cap->checkCap($this->data[$keyData])) {

                $this->error = [$keyData, 'err_bot'];

                return false;

            }

        }

    }

    private function email($list)
    {

        foreach ($list as $value) {

            $keyData = $value[0];

            if (filter_var($this->data[$keyData], FILTER_VALIDATE_EMAIL) === false) {

                $this->error = [$keyData, 'wrong_email', 'плохой email'];

                return false;
            }

        }

        return true;
    }

    private function lengthMax($list)
    {

        foreach ($list as $value) {

            $strLen = ! empty($value[2]) ? $value[2] : 16;

            $keyData = $value[0];

            if ($strLen < mb_strlen($this->data[$keyData], 'utf8')) {

                $this->error = [$keyData, 'max_length', 'длинный'];

                return false;
            }

        }

    }

    private function lengthMin($list)
    {

        foreach ($list as $value) {

            $strLen = ! empty($value[1]) ? $value[1] : $this->strLen;

            $keyData = $value[0];

            if ($strLen > mb_strlen($this->data[$keyData], 'utf8')) {

                $this->error = [$keyData, 'min_length', 'короткий'];

                return false;
            }

        }

    }

    private function matches($list)
    {
        $count = count($list);

        if (1 < $count) {

            foreach ($list as $value) {

                $keyData = $value[0];

                if (isset($this->data[$keyData])) {

                    $arr[$keyData] = $this->data[$keyData];
                }

            }

            $arrCount = (array_count_values($arr));

            $key = array_shift($arr);

            if ($arrCount[$key] != $count) {

                $this->error = [$keyData, 'not_match'];

                return false;
            }

        }

    }

    private function pasword($list)
    {

        if (false === $this->lengthMin($list)) {return false;}

        if (false === $this->lengthMax($list)) {return false;}

        foreach ($list as $value) {

            $keyData = $value[0];

            if ( ! preg_match('/^(?=.*[a-z])(?=.*\d)[a-zA-Z\d]+$/', $this->data[$keyData])) {

                $this->error = [$keyData, 'wrong_pass'];

                return false;
            }

            //  if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]+$/', $this->data[$keyData])) {

            //     $this->error = [$keyData, 'wrong_pass'];

            //     return false;
            // }

        }

    }

    private function required($list)
    {

        foreach ($list as $value) {

            $keyData = $value[0];

            if ('' == $this->data[$keyData]) {

                $this->error = [$keyData, 'empty'];

                return false;
            }

        }

    }

}
