<?php

function debug(...$arr)
{
    // var_dump($arr);echo'<br>';
    if (isset($arr[1])) {

        if (is_string($arr[0])) {
            $mark = ' => ';
        } else {
            $mark = ($arr[0] == $arr[1]) ? ' = ' : ' &ne; ';
        }

        $report = print_r($arr[0],true) . $mark;


        if (is_array($arr[1])) {
            $data = $arr[1];
        } else {
            $data = $arr[1] ?: 'null';
        }

        echo '<pre>' . $report . print_r($data, true) . '</pre>';
    } else {

        if (is_string($arr[0])) {

            echo 'debug => "' . $arr[0] . '"<br>';

        } else {

            echo '<pre>' . print_r($arr[0], true) . '</pre>';
        }

    }

    // traceClass(debug_backtrace());
}

function traceClass($arr)
{

    $caller = isset($arr[1])? $arr[1] : $arr[0];

    echo "Called by  \" {$caller['function']} \"";
    if (isset($caller['class'])) {
        echo " in  \"{$caller['class']}\"";
    }
    else{
        echo " in  \"{$caller['file']}\"";
    }
    echo " line: {$arr[0]['line']}";

}

function redirect($go_url = '')
{

    header('Cache-Control: public, no-cache="set-cookie"');
    header('Expires: 0');
    header('Pragma: no-cache');
    header("Location: " . DOMAIN . $go_url);

    exit;

    // echo '<meta http-equiv="refresh" content="0; url='.DOMAIN.$go_url.'">'; exit;
}

function err404()
{

    http_response_code(404);

    require_once WEB_DIR . '/404.php';exit;
}

function errAjax($report)
{

    echo json_encode(array('err' => $report));

    exit;
}

function Error($text)
{

    lib\Verifycation::$error .= $text;
}

function Report($text)
{

    lib\Verifycation::$report = $text;
}

function getIdUser()
{

    if (!empty($_SESSION['id_user'])) {

        return $_SESSION['id_user'];

    } else {

        die('exit');
    }
}

function setIdUser($id_user)
{

    $_SESSION['id_user'] = $id_user;
}
