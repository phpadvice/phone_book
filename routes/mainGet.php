<?php
use system\core\Router;

Router::get('^$',['controller' => 'Home', 'action' =>'run', 'method'=>'GET'] );

Router::get(LOGIN_URL, ['controller' => 'Users','action' =>'login']);

Router::get('captcha.png', ['controller' => 'Users','action' =>'captcha']);

Router::get(REG_URL, ['controller' => 'Users','action' =>'reg']);

Router::get('tmp{hesh_tmp}', ['controller' => 'Users','action' =>'confirm']);

// Router::get('c{id}/{alias}', ['controller' => 'page']);

// Router::get('{module}/all/{alias}', ['controller' => 'module\page', 'action' =>'view']);

// Router::get('{name}/c{idnew}', ['controller' => 'category', 'action' =>'view']);




