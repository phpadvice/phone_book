<?php
namespace app\models;

use system\core\Model;

/**
 *
 */
class User extends Model
{
    public $formData = [
        'email'   => '',
        'pass'    => '',
        'confirm' => '',
    ];

    public $rules = [

        'required' => [
            ['email'],
            ['pass'],
            ['confirm'],
        ],
        'email'    => [
            ['email'],
        ],
        'pasword'  => [
            ['pass', 5, 16],
            ['confirm', 5, 16],
        ],
        'matches'  => [
            ['pass'],
            ['confirm'],

        ],
    ];

    public $table = 'users';

    public function addNewUser()
    {
        return $this->insert([

            'pass'     => [hash('sha256', $this->formData['pass'])],
            'email'    => [$this->formData['email']],
            'tmp_hesh' => [$this->formData['tmp_hash'], false],

            'confirm'  => ['0', true],
            'date_reg' => ['NOW()', true],

        ]);

    }

    public function getTmpHesh($hesh_tmp)
    {

        if ($res = $this->findOne($hesh_tmp, 'tmp_hesh')) {

            return $res[0]['id_user'];
        }

        return false;
    }

    public function getUserLogin($email)
    {

        $arr = [

            'pass'  => [hash('sha256', $this->formData['pass'])],
            'email' => [$this->formData['email']],

        ];

        if ($res = $this->select($arr)) {

            return $res[0]['id_user'];
        }

        return false;

    }

    public function setConfirm($id)
    {
        $arr['fields'] = [

            'confirm'  => ['1', true],
            'tmp_hesh' => ['0', true],

        ];
        $arr['where'] = [

            'id_user' => [$id, false],
        ];

        $this->update($arr);
    }

}
