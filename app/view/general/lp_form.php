<div class="min-vh-100 align-items-center d-flex justify-content-center">

<div style="margin-top: -90px;" class="shadow p-4 bg-white rounded p-3" >

<form name="" action="" method="post" class="e-submit" data-func="validationAuth">


  <div class="form-group">

    <label class="h3 text-primary">Форма авторизации</label>

    <small id="emailHelp" class="form-text <?=$color_report?>">
        <?=$report.$error?>
    </small>

  </div>


  <div class="form-group">
    <label for="inputEmail"><?=NEW_EMAIL?></label>
    <input type="text" class="form-control" id="inputEmail"  name="email" novalidate>
    <small class="form-text text-muted">
    введите почту
    </small>

  </div>
  <div class="form-group">
    <label for="inputPass"><?=NEW_PASS?></label>
    <input name="pass" type="password" class="form-control" id="inputPass">
  </div>


  <div class="form-group">

    <label for="inputNorobot">Символы с картинки  <img src="/captcha.png" /></label>
<br>
    <input type="text" id="inputNorobot" class="form-control" name="norobot" />


  </div>

  <div class="d-flex justify-content-between">

    <button type="submit" id="submitBtn" name="btn_login" class="btn btn-primary" disabled>Вход</button>

    <a href="<?=DOMAIN?>/reg/" class="btn btn-link">Регистрация</a>

  </div>

</form>

</div>

</div>
