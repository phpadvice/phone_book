<div class="min-vh-100 align-items-center d-flex justify-content-center">

<div style="margin-top: -90px;" class="shadow p-4 bg-white rounded p-3" >

<form name="" action="/reg" method="post" class="e-submit" data-func="validationReg">

  <div class="form-group">

    <label class="h3 text-primary">Форма регистрации</label>

    <small id="emailHelp" class="form-text <?=$color_report?>">
        <?=$report.$error?>
    </small>

  </div>

  <div class="form-group">
    <label for="inputEmail"><?=NEW_EMAIL?></label>
    <input type="text" class="form-control" id="inputEmail"  name="email" novalidate value="<?=$email?>">
   <small class="form-text text-muted">
    введите почту
    </small>
  </div>
  <div class="form-group">
    <label for="inputPass"><?=NEW_PASS?></label>
    <input name="pass" type="password" class="form-control" id="inputPass">
    <small class="form-text text-muted">цифры +  латинские буквы</small>
  </div>
  <div class="form-group">
    <label for="inputPass2"><?=PASS_CONF?></label>
    <input name="confirm" type="password" class="form-control" id="inputPass2">

  </div>

  <div class="d-flex justify-content-between">

    <button type="submit" id="submitBtn" name="btn_reg" class="btn btn-primary" disabled=""><?=BTN_ADD?></button>
     <a href="<?=DOMAIN?>/<?=LOGIN_URL?>/" class="btn btn-link">Авторизация</a>

  </div>

</form>

</div>

</div>

