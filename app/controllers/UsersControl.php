<?php
namespace app\controllers;

use app\models\User;
use system\core\Controller;
use system\core\Router;
use system\core\View;
use system\lib\Captcha;
use system\lib\SendMail;
use system\lib\VerifyTools;

/**
 *
 */
class UsersControl extends Controller
{
    /**
     * @var mixed
     */
    public $typeRender = parent::TYPE_FULL;

    /**
     * @return null
     */
    public function addAction()
    {

        $modelUser = new User;

        $modelUser->upload();

        if (false === $modelUser->validateForm()) {

            $this->formErrReg($modelUser->formData);

            $this->setErr($modelUser->errLengMessage());

        } else {

            $this->addDataTmpHesh($modelUser);

            if (false === $modelUser->addNewUser()) {

                $this->setErr($modelUser->errDbError());

                $this->formErrReg($modelUser->formData);

                return false;
            }

            $this->sendMail($modelUser->formData);

        }

    }

    public function captchaAction()
    {
        $cap = new Captcha;

        $cap->run();

        exit;
    }

    public function confirmAction()
    {
        $this->view = LOGIN_URL;

        $modelUser = new User;

        if ($id = $modelUser->getTmpHesh(Router::getRoute()['hesh_tmp'])) {

            $modelUser->setConfirm($id);

            redirect("/" . LOGIN_URL . "/");
        }

        redirect("/404/");
    }

    public function loginAction()
    {
        $data = $this->setDataLp()->getFormData();
        // $this->formLogin();
        $this->formLogin($data);

    }

    public function lpAction()
    {
        $modelUser = $this->setDataLp();

        $modelUser->upload();

        if (false === $modelUser->validateForm()) {

            $this->formLogin($modelUser->formData);

            $this->setErr($modelUser->errLengMessage());

            $this->view = LOGIN_URL;

            return false;
        }

        if (false === $res = $modelUser->getUserLogin($modelUser->formData['email'])) {

            $this->setErr(ERR_LP);

            $this->formLogin($modelUser->formData);

            $this->view = LOGIN_URL;

            return false;
        }

        debug($res);die;

        // проверка в базе
        // запись в сессию

    }

    public function regAction()
    {
        View::setMeta('Форма регистрации');

        $this->formReg();

        $modelUser = new User;

        $this->add($modelUser->getFormData());

    }

    private function addDataTmpHesh(object $modelUser)
    {
        $modelUser->formData['tmp_hash'] = VerifyTools::tmpHesh($modelUser->formData['email']);
    }

    /**
     * @param $dataForm
     */
    private function formErrReg($dataForm)
    {
        View::setMeta('Ошибка регистрации');

        $this->view = REG_URL;

        $this->add($dataForm);

        // $this->setErr($modelUser->errLengMessage());
    }

    private function formLogin($data)
    {
        View::setMeta('Форма авторизации');

        $this->set([
            'report'       => 'пройдтте авторизацию',
            'color_report' => COLOR_REPORT,
        ]);

        $this->add($data);
    }

    private function formReg()
    {

        $this->set([
            'report'       => 'Зарегистрируйтесь',
            'color_report' => COLOR_REPORT,
        ]);
    }

    private function formRepoprSendMail($dataForm)
    {
        View::setMeta('Проверьте почту');

        $this->view = 'send_mail';

        // $this->add($dataForm);

        // $this->setErr($modelUser->errLengMessage());
    }

    private function sendMail($data)
    {

        $link_hesh = DOMAIN . '/tmp' . $data['tmp_hash'];

        $mails = new SendMail;
        $mails->sendMail($data['email'], $link_hesh);

        $this->formRepoprSendMail($data);
    }

    private function setDataLp()
    {
        $modelUser           = new User;
        $modelUser->formData = [
            'email' => '',
            'pass'  => '',
            'nobot' => '',
        ];

        $modelUser->rules = [

            'required' => [

                ['email'],
                ['pass'],
                ['nobot'],

            ],

            'email'    => [
                ['email'],
            ],
            'pasword'  => [
                ['pass', 5, 16],
            ],

            'captha'   => [
                ['nobot', 4],
            ],

        ];
        return $modelUser;
    }

}
