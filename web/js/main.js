
  // (function() {
  //   'use strict';
  //   window.addEventListener('load', function() {


  // var forms = document.getElementsByClassName('needs-validation');
  // var btn_submit = document.getElementById('btn_submit');


  // submitBtn.addEventListener('click', function(event) {
  //   var validation = Array.prototype.filter.call(forms, function(form) {

  //         if (form.checkValidity() === false) {
  //           event.preventDefault();
  //           //event.stopPropagation();
  //         }
  //         else{

  //           // $(".modal").modal("hide");

  //           contactObj.addContact('contact_form');
  //         }

  //         form.classList.add('was-validated');

  // });
  // });


  //   }, false);
  // })();

$("#submitBtn").bind("click", function(event){

    contactObj.addContact('contact_form');


});

/****************************************/

/**
 * [contactObj description]
 * @type {Object}
 */
const contactObj = {

  addContact(form){

    var oldForm = $('#'+form)[0];
    var file_data = $('#customFile').prop('files')[0];
    var form_data = new FormData(oldForm);

    form_data.append('file', file_data);
    form_data.append('act', 'add');

    $.ajax({
      url: '/ajax/',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(msg){

        var arr_msg = $.parseJSON(msg);

        if(!arr_msg['err']){



          // document.getElementById('#'+form).reset();
          contactObj.addFormReset(form);

          $("#addcontactModal").modal("hide");

          contactObj.upListContact(arr_msg['contact_data']);
        }
        else{

          alert(arr_msg['err']);
        }

      }

    });
  },


  addFormReset(form){

    jQuery('#'+form).get(0).reset();

    $('#name').removeClass('is-valid');
     $('#last_name').removeClass('is-valid');
      $('#inputPhone').removeClass('is-valid');
       $('#inputEmail').removeClass('is-valid');
$('#contact_form').removeClass('was-validated');


    $("#submitBtn").prop( 'disabled', true );

  },


  upListContact(arr_contact){

    var data_print;

    if(0 == arr_contact.length) {

      data_print = "записи отсутствуют"
    }
    else{

      // console.log("arr_contact",arr_contact);

      $.each(arr_contact, function(index,value){

          data_print+=`<tr id="id_`+value['id_pbook']+`">
                        <td>`+value['name']+`</td>
                        <td>`+value['last_name']+`</td>
                        <td>`+value['phone']+`</td>
                      </tr>`;
      });
    }

    $('#list_contacts').html(data_print);

  },
}





/**
 * [description]
 * @param  {[type]} event){                 var name [description]
 * @return {[type]}          [description]
 */
  $(".e-click").bind("click", function(event){

    var name =  $(this).data("func");

    if (typeof eClick[name] == 'function'){

     eClick[name](event);

    }
    else {

      console.log(' not name = '+$(this).attr('id'));

    }
  });


/**
 * [eClick description]
 * @type {Object}
 */
const eClick = {

  id_contact: 0,
  contact_data: [],




  sortData(event){

    var el_id = $(event.target).attr("id");

    $.ajax({
      type: "POST",
      url: "/ajax/",
      data: "act=sort&col_name="+el_id,
      success: function(msg){

       // console.log(msg);
        var arr_msg = $.parseJSON(msg);

        if(!arr_msg['err']){

        contactObj.upListContact(arr_msg['contact_data']);
        }

      }
    });

  },


  setID(event){

    // var el = event.target.parentNode;

    var el_id = $(event.target.parentNode).attr("id");

    if(-1 != el_id.indexOf('id_')){

      eClick.id_contact = el_id.split('_').pop();

      eClick.openContact();
    }
  },


  openContact(){

    $.ajax({
      type: "POST",
      url: "/ajax/",
      data: "act=open&id="+eClick.id_contact,
      success: function(msg){

          // console.log(msg);

        var arr_msg = $.parseJSON(msg);

        if(!arr_msg['err']){

          eClick.contact_data = arr_msg['contact_data'];

          eClick.setContact(eClick.contact_data);

        }

      }
    });


    $('#contact').modal('show');

  },


  setContact(data){

    let data_prn='';

    $('#img_contact').attr('src','/photo/'+data['photo']);

    for(let[key, value] of Object.entries(data)){

      $('#li_'+key).html(value);
    };

  },


  del_contact(){

   $.ajax({
      type: "POST",
      url: "/ajax/",
      data: "act=del&id_pbook="+eClick.id_contact,
      success: function(msg){

// console.log(msg);

        $('#contact').modal('hide');

        var arr_msg = $.parseJSON(msg);

        if(!arr_msg['err']){

        contactObj.upListContact(arr_msg['contact_data']);
        }

        // alert( "закрыть окно и обновить список(или удалить строку из списка) " + msg );
      }
    });
  },


  open_edit(){

    eClick.setEditContact(eClick.contact_data);

    $('#editcontactModal').modal('show');

    $('#btn_submit').prop( 'disabled', true );

  },


  setEditContact(data){

    let data_prn='';

    $('#edt_img_contact').attr('src','/photo/'+data['photo']);

    for(let[key, value] of Object.entries(data)){

      $('#edt_'+key).val(value);
    };

  },


  updateContact(){


    var oldForm = $('#edt_contact_form')[0];
    var file_data = $('#customFile').prop('files')[0];
    var form_data = new FormData(oldForm);

    form_data.append('file', file_data);
    form_data.append('act', 'upd');
    form_data.append('id_pbook', eClick.id_contact);

    $.ajax({
      url: '/ajax/',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function(msg){

        // console.log(msg);

        var arr_msg = $.parseJSON(msg);

        if(!arr_msg['err']){

          jQuery('#edt_contact_form').get(0).reset();

          $("#editcontactModal").modal("hide");

          contactObj.upListContact(arr_msg['contact_data']);
        }
        else{

          alert(arr_msg['err']);
        }

      }

    });

    validObj.valid_btn.length = 0;
  },

}






  $(".e-submit").bind("input", function(event){

    var name =  $(this).data("func");

    if (typeof validObj[name] == 'function'){

     validObj[name](event);

    }
    else {

      console.log(' not name = '+$(this).attr('id'));

    }
  });


const validObj = {

  valid_btn: [],


  validationEdt(){

    validObj.validationName('#edt_name', 0);

    validObj.validationLName('#edt_last_name', 1);

    validObj.validationPhone('#edt_phone', 2);

    validObj.validationEmail('#edt_email', 3);


    validObj.validationBtn('#btn_submit');

  },


  validationAdd(){

  validObj.validationName('#name', 0);

  validObj.validationLName('#last_name', 1);

  validObj.validationPhone('#inputPhone', 2);

  validObj.validationEmail('#inputEmail', 3);


  validObj.validationBtn('#submitBtn');


  },

  validationBtn(btn_id){

    if(validObj.valid_btn.includes(0))
    {
      $(btn_id).prop( 'disabled', true );

    }
    else{
      $(btn_id).prop( 'disabled', false );
    }

  },


  validationAuth(){

    validObj.validationEmail('#inputEmail', 0);

    validObj.validationPass('#inputPass', 1);

    validObj.validationNobot('#inputNobot', 2);

    validObj.validationBtn('#submitBtn');

  },


   validationReg(){

    validObj.validationEmail('#inputEmail', 0);

    validObj.validationPass('#inputPass', 1);

    validObj.validationPass2('#inputPass', '#inputPass2', 2);

    validObj.validationBtn('#submitBtn');
  },

    validationGood(id, key){

       $(id).addClass('is-valid');

      $(id).removeClass('is-invalid');
       //$("#submitBtn").prop( 'disabled', false );

      validObj.valid_btn[key] = 1;

    },

    validationBad(id, key){

      $(id).addClass('is-invalid');
      $(id).removeClass('is-valid');
      //$("#submitBtn").prop( 'disabled', true );
      validObj.valid_btn[key] = 0;

    },



  validationEmail(id, key){

    var value = $(id).val();

    var re = '^[-\\w.]+@([A-z0-9]+\.)+[A-z]{2,}$';

    if(value.match(re)) {

      validObj.validationGood(id, key);
    }
    else{

      validObj.validationBad(id, key);
    }
  },


  validationPass(id, key){

    var value = $(id,).val();

    var re = '^(?=.*\\d)(?=.*[a-z])(?!\\*\\s).*$';

    if(value.match(re) && value.match('^[a-zA-Z\\d]*$')) {

      validObj.validationGood(id, key);
    }
    else{

      validObj.validationBad(id, key);
    }
  },


  validationPass2(id1, id2 , key){

    var value = $(id1,).val();

    var value2 = $(id2,).val();

    if(0 != value2.length && value == value2) {


      validObj.validationGood(id2, key);
    }
    else{

      validObj.validationBad(id2, key);
    }
  },


  validationNobot(id, key){

    var value = $(id,).val();

    if( 4 == value.length ){

      validObj.validationGood(id, key);
    }
    else{

      validObj.validationBad(id, key);
    }
  },


  validationPhone(id, key){

    var value = $(id,).val();

    var re = '^[\+]?[\\d]{5,12}$';

    if(value.match(re) && value.match('^[-\\+\\d]*$')) {

      validObj.validationGood(id, key);
    }
    else{

      validObj.validationBad(id, key);
    }

  },

  validationName(id, key){

    var value = $(id,).val();

    if(value && value.length>2) {

      validObj.validationGood(id, key);
    }
    else{

      validObj.validationBad(id, key);
    }

  },

    validationLName(id, key){

    var value = $(id,).val();

    if(value && value.length>2) {

      validObj.validationGood(id, key);
    }
    else{

      validObj.validationBad(id, key);
    }

  },

}



