<?php

use system\core\Router;

// @session_start();

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', true);
ini_set('error_reporting', E_ALL);

$query = rtrim($_SERVER['QUERY_STRING'], '/');
/**
 * установка констант
 */
define('WEB_DIR', __DIR__);
define('HOME_DIR', dirname(__DIR__));

/**
 *  загрузка конфига
 */
require_once HOME_DIR . '/system/config/config.php';
/**
 * запуск сессии
 */
require_once CORE . '/SessionStart.php';
/**
 * подключение автозагрузки
 */
require_once CORE . '/Autoloader.php';
/**
 * глобальные функции
 */
require_once LIB . '/Tools.php';
/**
 * загрузка языка
 */
require_once HOME_DIR . '/app/lengs/ru/report.php';
/**
 * Старт
 */
Router::run($query);

// debug(Router::getParams());
// debug(Router::getRoute());
